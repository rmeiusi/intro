public class Grade {

   public static void main (String[] p) {
      double percent;
      while (true) {
         System.out.print ("Input percentage: ");
         percent = TextIO.getlnDouble();
         System.out.print (percent + ": ");
         System.out.println (grade (percent));
      } // while
   } // main


   public static String grade (double d) {
      String grade = "not defined";
      if (d<0)
    	  throw new IllegalArgumentException ("negatiivne protsent:" + d);
      if (d>100)
    	  throw	new IllegalArgumentException ("liiga suur protsent:" + d);
      if (d>=90.5)
    	  return "excellent";
      if (d>=80.5)
    	  return "very good";
      if (d>=70.5)
    	  return "good";
      if (d>=60.5)
    	  return "satisfactory";
      if (d>=50.5) 
    	  return "sufficient";
      if (d>=0) 
    	  return "fail";
      return grade;
   }
}

